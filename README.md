#docker-debian-packaging
Build debian packages using docker:

* It enables to build from linux, windows and osx
* Easily reproducible build infrastructure
* Does not need any secrets (like private keys, etc.) to be contained in the image nor in the git repository.

## Requirements
* docker
* docker-compose

## Usage

* `docker-compose build` - to build the initial docker image which is used for packaging
* `docker-compose run login` - to create a new container for packaging and drop to a shell

## Configuration

* `mrconfig` - contains all your git repository on Alioth, salsa or elsewhere.
* `.env` - contains local ENV variables like the `SSH_FOLDER` or the `GPG_FOLDER`
