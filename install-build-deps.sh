#!/bin/bash

set -eu

if [ ! -f debian/control ]
then
  echo -e "Not in a valid debian package directory"
  exit 1
fi

cd debian
mk-build-deps control
set +e
sudo dpkg -i *.deb
set -e
sudo apt-get update
sudo apt-get install -f -y
rm -f *.deb
cd ..
