#!/bin/bash
set -eu

mkdir -p $HOME/.ssh
cp -a $HOME/ssh/* $HOME/.ssh
chmod 700 $HOME/.ssh/*

sudo install -d -m 2775 -o 1234 -g 1234 /var/cache/pbuilder/ccache

mkdir -p $HOME/packages
sudo chown -R builder:builder $HOME/packages
mkdir -p $HOME/add-packages

git config --global user.email "$GIT_EMAIL"
git config --global user.name "$GIT_NAME"

exec bash
