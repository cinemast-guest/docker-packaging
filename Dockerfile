FROM debian:sid

RUN DEBIAN_FRONTEND=noninteractive apt-get update -y && apt-get upgrade -y \
  && apt-get install -y devscripts myrepos sudo tig git-buildpackage ccache \
      duck quilt cowbuilder vim \
  && apt-get clean -y && rm -rf /var/lib/apt/lists/*
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y devscripts

COPY sudoers /etc/sudoers
RUN useradd -ms /bin/bash builder && echo "builder:builder" | chpasswd && adduser builder sudo
WORKDIR /home/builder
#RUN mkdir -p /home/builder/add-packages && touch /home/builder/add-packages/Packages
RUN ln -s /home/builder/.pbuilderrc /root/.pbuilderrc

COPY install-build-deps.sh /usr/local/bin/install-build-deps
RUN chmod a+x /usr/local/bin/install-build-deps


COPY entrypoint.sh /entrypoint.sh
RUN chmod a+x /entrypoint.sh
ENTRYPOINT /entrypoint.sh
USER builder
COPY quiltrc /home/builder/.quiltrc

VOLUME ["/var/cache/pbuilder", "/home/builder/packages"]
